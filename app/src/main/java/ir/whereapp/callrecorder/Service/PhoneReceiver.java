package ir.whereapp.callrecorder.Service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneReceiver extends BroadcastReceiver {
    private static boolean incomingFlag = false;
    private static String incoming_number = null;
    ServiceRecorder serviceRecorder;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Developer-Rahimi", "PhoneReceiver: " + "Received");
        String action = intent.getAction();
        if (!action.equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED) &&
                !action.equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            Log.e("Developer-Rahimi", "PhoneReceiver: Received unexpected intent: " + action);
            return;
        }
            TelephonyManager tm = (TelephonyManager)context.getSystemService(Service.TELEPHONY_SERVICE);
            switch (tm.getCallState()) {

                case TelephonyManager.CALL_STATE_RINGING:
                    Intent i=new Intent(context, ServiceRecorder.class);
                    i.putExtra("Action","Ring");
                    context.startService(i);
                    incomingFlag = true;

                    incoming_number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

                    Log.i("Developer-Rahimi", "RINGING :"+ incoming_number);
                    Log.i("Developer-Rahimi", "PhoneReceiver: " + "Received1");

                    break;

                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Intent OFFHOOK=new Intent(context, ServiceRecorder.class);
                    OFFHOOK.putExtra("Action","StartRec");
                    context.startService(OFFHOOK);
                    if(incomingFlag){

                        Log.i("Developer-Rahimi", "incoming ACCEPT :"+ incoming_number);
                        Log.i("Developer-Rahimi", "PhoneReceiver: " + "Received2");

                    }

                    break;



                case TelephonyManager.CALL_STATE_IDLE:
                    Intent IDLE=new Intent(context, ServiceRecorder.class);
                    IDLE.putExtra("Action","StopRec");
                    context.startService(IDLE);
                    if(incomingFlag){

                        Log.i("Developer-Rahimi", "incoming IDLE");
                        Log.i("Developer-Rahimi", "PhoneReceiver: " + "Received3");

                    }

                    break;

            }

    }
}
