package ir.whereapp.callrecorder.Service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class ServiceRecorder extends Service {
    String root= Environment.getExternalStorageDirectory().getAbsolutePath()+"/RecordVoice";
    ServiceRecorder serviceRecorder;
    private final IBinder mBinder = new LocalBinder();
    private final Random mGenerator = new Random();
    MediaRecorder recorder;
    String filename;
    public class LocalBinder extends Binder {
        public ServiceRecorder getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServiceRecorder.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** method for clients */
    public int getRandomNumber() {
        return mGenerator.nextInt(100);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Developer-Rahimi", "Service: " + "Start");
        File dir=new File(root);
        if(!dir.exists())
            dir.mkdir();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        try {
            String action=intent.getStringExtra("Action");
            Log.i("Developer-Rahimi",action);
            if(action.equals("Ring")){
                Log.i("Developer-Rahimi", "Ring");
                InitRec();
            }
            else if(action.equals("StartRec")){
                Log.i("Developer-Rahimi", "Start");
                try {
                    recorder.prepare();
                } catch (IOException e) {
                    Log.i("Developer-Rahimi", "prepare() failed");
                }
                recorder.start();
            }else if(action.equals("StopRec")){
                Log.i("Developer-Rahimi", "Stop");
                recorder.stop();
                recorder.release();
                recorder = null;
            }
        }catch (Exception e){
            Log.i("Developer-Rahimi","Error:"+e.getMessage());
        }
    }
    public void InitRec(){
        filename=root+"/001.wav";
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(filename);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
    }
}
