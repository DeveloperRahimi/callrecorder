package ir.whereapp.callrecorder;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import ir.whereapp.callrecorder.Others.RuntimePermissionsActivity;
import ir.whereapp.callrecorder.Service.ServiceRecorder;

public class MainActivity extends RuntimePermissionsActivity {
    Context context;
    ServiceRecorder serviceRecorder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;
        GenratePermission();
        Intent intent=new Intent(this, ServiceRecorder.class);
        startService(intent);
        //bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
    }
    private void GenratePermission() {
        MainActivity.super.requestAppPermissions(new String[]{Manifest.permission.READ_PHONE_STATE,Manifest.permission.RECORD_AUDIO,Manifest.permission.PROCESS_OUTGOING_CALLS,Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 1);
    }
    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    @Override
    public void onPermissionsDeny(int requestCode) {
        switch (requestCode){
            case 1:
                finish();
            break;
        }
    }
    public ServiceConnection myConnection = new ServiceConnection(){

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // TODO Auto-generated method stub
            ServiceRecorder.LocalBinder binder = (ServiceRecorder.LocalBinder) service;
            serviceRecorder = binder.getService();
            Log.i("Developer-Rahimi","Connected");
           //Log.i("Developer-Rahimi",serviceRecorder.getRandomNumber()+"");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i("Developer-Rahimi","DisConnected");
        }

    };
}
